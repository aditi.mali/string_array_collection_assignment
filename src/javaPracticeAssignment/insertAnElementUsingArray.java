package javaPracticeAssignment;

import java.util.Arrays;

public class insertAnElementUsingArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] my_arr = {4, 14, 68, 105, 360, 56, 77, 18, 29, 49};

	    // Insert an element in 3rd position of the array (index->2, value->5)
	   
	   int Index_position = 2;
	   int newValue    = 5;

	  System.out.println("Original Array : "+Arrays.toString(my_arr));     
	   
	  for(int i=my_arr.length-1; i > Index_position; i--)
	  {
	    my_arr[i] = my_arr[i-1];
	   }
	   my_arr[Index_position] = newValue;
	   System.out.println("New Array: "+Arrays.toString(my_arr));

	}

}
